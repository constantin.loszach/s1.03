Gitlab : https://gitlab.com/constantin.loszach/s1.03

Démarches d'installation :

Installation Xubuntu :
    Choix :
        J'ai choisi d'installer Xubuntu sur un veil ordinateur 
        car il était inutilisé et je ne risquais rien (perte de fichiers par exemple).
        J'ai choisi Xubuntu car une clef a été fournie par l'IUT.

    L'installation :
        - Brancher la clef
        - Ouvrir le BIOS (appuyer sur f2 au démarrage pour moi)
        - Allez dans les option pour changer la priorité de boot et sélectionner la clef USB. (de mon coté l'option se nomme : Boot Sequence)
        - Après avoir changer la priorité appliquer les changements et redémarrer la machine.
        - Losrqu'on se retrouve sur l'interface Xubuntu d'essai, double cliquer sur "Install Xubuntu"
        - Sélectionner la langue souhaitée.
        - Sélectionner la configuration du clavier voulue
        - Ensuite laisser cochée la première case et cocher la deuxième case pour pouvoir installer les logiciels tiers comme par exemple les pilotes graphiques nvidia.
        - Créer son mot de passe et le confirmer
        - Sléectionner l'option "Effacer le disque et installer Xubuntu" pour une installation complète.
        - Installer
        - Sélectionner le fuseau horraire de Paris
        - Completer les informations demandées
        - Continuer l'installation et patienter
        - Redémarrer quand demandé
        - Enlever la clef et appuyer sur la touche Enter comme demandé
        - Appuyer sur entrer en sélectionnant l'option "continue boot"
        

Installation de vscode :
    - Exécuter la commande suivante dans un terminal : sudo snap install code --classic

Installation de python :
    - Exécuter cette commande dans un terminal : sudo pat install python3

Installation de java :
    - Taper la commande suivante dans un terminal : sudo apt install default-jdk

Installation de docker : 
    - Taper cette commande dans un terminal : sudo apt-get install docker.io
